import sys
import json
import time

from base64 import b64decode
from pathlib import Path

from gbc.controllers.gbcv1 import Gbc as GreenboneConntroller
from gbc.utils.constants import reportFormats
from gbc.utils import indent

class GbcService:
    
    controller = None
    
    def __init__(self):
        self.controller = GreenboneConntroller.getInstance()
    
    def _getRegistryViolations(self, description):
        report = []
        entries = description.split('| Value set in registry\n')[1].split('\n')
        for entry in entries:
            registryEntry = entry.split("|")[0].strip()
            present = entry.split("|")[1].strip()
            valueCheckedAgainst = entry.split("|")[2].strip()
            valueSetInRegistry = entry.split("|")[3].strip() if len(entry.split("|"))==4 else None
            report.append({"registryEntry": registryEntry, "present": present, "valueCheckedAgainst": valueCheckedAgainst, "valueSetInRegistry": valueSetInRegistry})

        return report

    def getReportFormats(self, ):
        return reportFormats

    def _getReportFormatsXml(self, ):
        response_xml = self.controller.send_command( '<get_report_formats />')
        return response_xml

    def _getReportFormat(self, report_format_id):
        response_xml = self.controller.send_command( '<get_report_formats report_format_id="%s" />' % report_format_id )
        return response_xml

    def _getReportByFormatXml(self, report_id, report_format_id):
        response_xml = self.controller.send_command( '<get_reports report_id="%s" format_id="%s" filter="levels=hml" />' % (report_id, report_format_id) )
        return response_xml

    def _getResultsXml(self, report_id):
        response_xml = self.controller.send_command( '<get_results filter="report_id=%s apply_overrides=0 levels=hml rows=100 min_qod=70 first=1 sort-reverse=severity" ignore_pagination="0" />' % (report_id) )
        return response_xml

    def _getResultXml(self, result_id):
        response_xml = self.controller.send_command('<get_results result_id="%s" />' % (result_id) )
        return response_xml

    def _getReportsXml(self, filter):
        response_xml = self.controller.send_command('<get_reports report_filter="min_qod=70 apply_overrides=1 rows=10 sort-reverse=date first=%s" ignore_pagination="0" details="0" />' % (filter["first"]))
        return response_xml

    def _getReportXml(self, report_id):
        response_xml = self.controller.send_command('<get_reports report_id="%s" details="0" />' % (report_id) )
        return response_xml

    def _getReportWithDetailsXml(report_id):
        response_xml = self.controller.send_command('<get_reports report_id="%s" />' % (report_id) )
        return response_xml

    def _getSeverityRanges(self, sevRangeList):
        sevRanges = []
        for sevRange in sevRangeList:
            sevRanges.append({"name": "".join(sevRange.xpath("name/text()")), "min": float("".join(sevRange.xpath("min/text()"))), "max": float("".join(sevRange.xpath("max/text()")))})
        return sevRanges

    def _getSeverityLevel(self, severityClassXml, severity):
        sevRangeList = severityClassXml.xpath('severity_range')
        sevRanges = self._getSeverityRanges(sevRangeList)
        for sevRange in sevRanges:
            if severity <= sevRange["max"] and severity >= sevRange["min"]:
                return sevRange["name"]

    def getReports(self, filter):
        response_xml = self._getReportsXml(filter)
        reports_xml = response_xml.xpath('report')

        reports = []

        for report_xml in reports_xml:
            report_id = "".join(report_xml.xpath('@id'))
            name = "".join(report_xml.xpath('name/text()'))
            owner = "".join(report_xml.xpath('owner/name/text()'))
            creation_time = "".join(report_xml.xpath('creation_time/text()'))
            modification_time = "".join(report_xml.xpath('modification_time/text()'))
            task_name = "".join(report_xml.xpath('task/name/text()'))

            report_details_xml = report_xml.xpath('report')[0]
            severity = "".join(report_details_xml.xpath('severity/full/text()'))
            severity_level = self._getSeverityLevel(report_details_xml.xpath('severity_class')[0], float(severity))
            status = "".join(report_details_xml.xpath('scan_run_status/text()'))

            reports.append(
                {
                    "id": report_id,
                    "name": name,
                    "status": status,
                    "taskName": task_name,
                    "severity": severity,
                    "severityLevel": severity_level,
                }
            )
        return reports

    def getResults(self, report_id):
        response_xml = self._getResultsXml(report_id)
        results_xml = response_xml.xpath('result')

        results = []
        for result_xml in results_xml:
            r_id = "".join(result_xml.xpath('@id'))
            r_name = "".join(result_xml.xpath('name/text()'))
            r_host_ip = "".join(result_xml.xpath('host/text()'))
            r_port = "".join(result_xml.xpath('port/text()'))
            r_threat = "".join(result_xml.xpath('threat/text()'))

            r_severity = "".join(result_xml.xpath('severity/text()'))
            #severityClassXml = report_details_xml.xpath('severity_class')[0]
            #r_severity_level = _getSeverityLevel(severityClassXml, float(r_severity))

            r_qod = "".join(result_xml.xpath('qod/value/text()'))
            r_description = "".join(result_xml.xpath('description/text()'))
            r_registry_violations = self._getRegistryViolations(r_description) if (r_name == "Windows Registry Check: Violations") else []

            results.append(
                {
                    "id": r_id,
                    "name": r_name,
                    "hostIp": r_host_ip,
                    "port": r_port,
                    "threat": r_threat,
                    "severity": r_severity,
                    "qod": r_qod,
                    "description": r_description,
                    "registryViolations": r_registry_violations,
                }
            )

        return results

    def getReport(self, report_id):
        response_xml = self._getReportXml(report_id)
        reports_xml = response_xml.xpath('report')
        report_xml = reports_xml[0]

        report_id = "".join(report_xml.xpath('@id'))
        name = "".join(report_xml.xpath('name/text()'))
        owner = "".join(report_xml.xpath('owner/name/text()'))
        creation_time = "".join(report_xml.xpath('creation_time/text()'))
        modification_time = "".join(report_xml.xpath('modification_time/text()'))
        task_name = "".join(report_xml.xpath('task/name/text()'))

        report_details_xml = report_xml.xpath('report')[0]

        severity = "".join(report_details_xml.xpath('severity/full/text()'))
        severityClassXml = report_details_xml.xpath('severity_class')[0]
        severity_level = self._getSeverityLevel(severityClassXml, float(severity))

        status = "".join(report_details_xml.xpath('scan_run_status/text()'))

        return {
            "id": report_id,
            "name": name,
            "status": status,
            "creationTime": creation_time,
            "modifiedTime": modification_time,
            "owner": owner,
            "taskName": task_name,
            "severity": severity,
            "severityLevel": severity_level,
        }

    def getReportByFormat(self, report_id, report_format_id):
        reportFormat = self.getReportFormat(report_format_id)
        report = self.getReport(report_id)

        fileName = "/reports/%s.%s" % (report_id, reportFormat['extension'])

        response_xml = self._getReportByFormatXml(report_id, report_format_id)

        report_element = response_xml[0]
        # get the full content of the report element
        content = "".join(report_element.itertext())
        content = content.split("%s%s" % (report['taskName'], reportFormat['name']))[1]

        # convert content to 8-bit ASCII bytes
        binary_base64_encoded = content.encode('ascii')

        # decode base64
        binary = b64decode(binary_base64_encoded)

        # write to file and support ~ in filename path
        path = Path(fileName).expanduser()

        path.write_bytes(binary)

    def getResult(self, result_id):
        response_xml = self._getResultXml(result_id)
        result_xml = response_xml.xpath('result')[0]

        report_element = response_xml[0]
        content = "".join(report_element.itertext())

        result_id = "".join(result_xml.xpath('@id'))
        name = "".join(result_xml.xpath('name/text()'))
        host_ip = "".join(result_xml.xpath('host/text()'))
        port = "".join(result_xml.xpath('port/text()'))
        threat = "".join(result_xml.xpath('threat/text()'))
        severity = "".join(result_xml.xpath('severity/text()'))
        qod = "".join(result_xml.xpath('qod/value/text()'))
        description = "".join(result_xml.xpath('description/text()'))
        registry_violations = self._getRegistryViolations(description) if (name == "Windows Registry Check: Violations") else []

        return {
            "id": result_id,
            "name": name,
            "hostIp": host_ip,
            "port": port,
            "threat": threat,
            "severity": severity,
            "qod": qod,
            "description": description,
            "content": content,
            "registryViolations": registry_violations
        }

    def getReportFormat(self, report_format_id):
        #response_xml = self._getReportFormat(report_format_id)
        #report_format_xml = response_xml.xpath('report_format')[0]

        #name = ''.join(report_format_xml.xpath('name/text()'))
        #report_format_id = "".join(report_format_xml.xpath('@id'))
        #extension = ''.join(report_format_xml.xpath('extension/text()'))

        #return {'id': report_format_id, 'name': name, 'extension': extension}
        for reportFormat in reportFormats:
            if (reportFormat["id"] == report_format_id):
                return reportFormat

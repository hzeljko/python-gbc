import sys
import json
import time

from base64 import b64decode
from pathlib import Path

from controllers.gbcv1 import Gbc as GreenboneConntroller
from utils.constants import reportFormats
from utils import indent, stopwatch

from service import GbcService

service = GbcService()


indent2 = indent(0)
reports = stopwatch(service.getReports)

# get single reports
for r in reports:
    indent2 = indent(2)
    report = stopwatch(service.getReport, r['id'])
    
#    for reportFormat in reportFormats:
#        indent2 = indent(4)
#        stopwatch(service.getReportByFormat, report['id'], reportFormat['id'])
    
    indent2 = indent(4)
    results = stopwatch(service.getResults, report['id'])
    
    # get single result
    for rs in results:
        indent2 = indent(6)
        report = stopwatch(service.getResult, rs['id'])

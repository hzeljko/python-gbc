reportFormats = [
#    {
#        "id": "5057e5cc-b825-11e4-9d0e-28d24461215b",
#        "name": "Anonymous XML",
#        "extension": "xml"
#    },
    {
        "id": "910200ca-dc05-11e1-954f-406186ea4fc5",
        "name": "ARF",
        "extension": "xml"
    },
    {
        "id": "5ceff8ba-1f62-11e1-ab9f-406186ea4fc5",
        "name": "CPE",
        "extension": "csv"
    },
    {
        "id": "9087b18c-626c-11e3-8892-406186ea4fc5",
        "name": "CSV Hosts",
        "extension": "csv"
    },
    {
        "id": "c1645568-627a-11e3-a660-406186ea4fc5",
        "name": "CSV Results",
        "extension": "csv"
    },
    {
        "id": "ffa123c9-a2d2-409e-bbbb-a6c1385dbeaa",
        "name": "GSR HTML",
        "extension": "html"
    },
    {
        "id": "35ba7077-dc85-42ef-87c9-b0eda7e903b6",
        "name": "GSR PDF",
        "extension": "pdf"
    },
    {
        "id": "ebbc7f34-8ae5-11e1-b07b-001f29eadec8",
        "name": "GXR PDF",
        "extension": "pdf"
    },
    {
        "id": "77bd6c4a-1f62-11e1-abf0-406186ea4fc5",
        "name": "ITG",
        "extension": "csv"
    },
    {
        "id": "a684c02c-b531-11e1-bdc2-406186ea4fc5",
        "name": "LaTeX",
        "extension": "tex"
    },
    {
        "id": "9ca6fe72-1f62-11e1-9e7c-406186ea4fc5",
        "name": "NBE",
        "extension": "nbe"
    },
    {
        "id": "c402cc3e-b531-11e1-9163-406186ea4fc5",
        "name": "PDF",
        "extension": "pdf"
    },
    {
        "id": "9e5e5deb-879e-4ecc-8be6-a71cd0875cdd",
        "name": "Topology SVG",
        "extension": "svg"
    },
    {
        "id": "a3810a62-1f62-11e1-9219-406186ea4fc5",
        "name": "TXT",
        "extension": "txt"
    },
    {
        "id": "c15ad349-bd8d-457a-880a-c7056532ee15",
        "name": "Verinice ISM",
        "extension": "vna"
    },
    {
        "id": "50c9950a-f326-11e4-800c-28d24461215b",
        "name": "Verinice ITG",
        "extension": "vna"
    },
    {
        "id": "a994b278-1f62-11e1-96ac-406186ea4fc5",
        "name": "XML",
        "extension": "xml"
    }
]
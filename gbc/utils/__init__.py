import time

class bcolors:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'

def indent(size):
    s = ""
    for i in range(0, size):
        s = "%s " % s
    return s

def stopwatch(f, *p):
    start_time = time.time()
    res = f(*p) if p else f()
    exe_time = round(time.time()-start_time, 3)
    color = ""
    if exe_time > 0.5:
        color = bcolors.YELLOW
    if exe_time > 1:
        color = bcolors.RED
    print(color + "%.3f: %s with %s" % (exe_time, f.__name__, p) + bcolors.ENDC)
    return res
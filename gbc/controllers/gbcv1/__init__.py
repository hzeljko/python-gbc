from gvm.transforms import EtreeCheckCommandTransform
from gvm.protocols.gmpv8 import Gmp
from gvm.connections import SSHConnection

from gbc.settings import (
    SSH_HOSTNAME,
    SSH_PORT,
    GVM_USERNAME,
    GVM_PASSWORD
)

import json

class Gbc:
    
    __instance = None
    
    connector = None
    
    @staticmethod 
    def getInstance():
        """ Static access method. """
        if Gbc.__instance == None:
            Gbc()
        return Gbc.__instance
    
    def __init__(self):
        if Gbc.__instance != None:
            raise Exception("Cannot create more than one Greenbone controllers")
        else:
            Gbc.__instance = self
            self._createController()
    
    def _createController(self):
        connection = SSHConnection(hostname=SSH_HOSTNAME, port=SSH_PORT)
        transform = EtreeCheckCommandTransform()
        
        self.connector = Gmp(connection, transform=transform)
        self._connect()
        self._authenticate()
    
    def _authenticate(self):
        self.connector.authenticate(GVM_USERNAME, GVM_PASSWORD)
        
    def _connect(self):
        self.connector.connect()
        
    def _disconnect(self):
        self.connector.disconnect()
        
    def send_command(self, command):
        return self.connector.send_command(command)
    
    def __str__(self):
        return json.dumps({
            "connector": {
                "hash": repr(self.connector),
                "protocol": Gmp.__name__ + "v" + ".".join(str(v) for v in Gmp.get_protocol_version()),
                "authenticated_user": GVM_USERNAME if self.connector.is_authenticated() else None,
                "connection": {
                    "name": self.connector._connection.__class__.__name__,
                    "hostname": self.connector._connection.hostname,
                    "port": self.connector._connection.port
                }
                
            }
        }, indent=2)
